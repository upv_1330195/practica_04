package course.examples.practica_04;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {


    public final static String EXTRA_MESSAGE1 = "course.exmples.practica_04_1";
    public final static String EXTRA_MESSAGE2 = "course.exmples.practica_04_2";
    public final static String EXTRA_MESSAGE3 = "course.exmples.practica_04_3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        TabHost tb = (TabHost)findViewById(R.id.tabHost);
        tb.setup();


        TabHost.TabSpec tab1 = tb.newTabSpec("tab1");
        TabHost.TabSpec tab2 = tb.newTabSpec("tab2");
        TabHost.TabSpec tab3 = tb.newTabSpec("tab3");

        tab1.setIndicator("Farenheit a Celsius");
        tab1.setContent(R.id.Conversion1);

        tab2.setIndicator("Area de un cuadrado");
        tab2.setContent(R.id.Conversion2);

        tab3.setIndicator("Millas a Kilometros");
        tab3.setContent(R.id.Conversion3);

        tb.addTab(tab1);
        tb.addTab(tab2);
        tb.addTab(tab3);

        /*
        Button bt1=(Button)findViewById(R.id.btnSend1);
        bt1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                EditText et1= (EditText)findViewById(R.id.edit_message1);
                //create an Intent object
                Intent intent1=new Intent(this, DisplayMessageActivity1.class);
                //add data to the Intent object
                intent1.putExtra("text1", et1.getText().toString());
                //start the second activity
                startActivity(intent1);
            }

        });
        */
        /*TabHost.TabSpec tab1 = tb.newTabSpec("Farenheit a Celsius");
        TabHost.TabSpec tab2 = tb.newTabSpec("Area de im cuadrado");
        TabHost.TabSpec tab3 = tb.newTabSpec("Millas a Kilometros");*/




    }


    public void enviarMensaje1(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity1.class);
        EditText editText = (EditText) findViewById(R.id.edit_message1);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE1, message);
        startActivity(intent);
    }

    public void enviarMensaje2(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity2.class);
        EditText editText = (EditText) findViewById(R.id.edit_message2);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE2, message);
        startActivity(intent);
    }

    public void enviarMensaje3(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity3.class);
        EditText editText = (EditText) findViewById(R.id.edit_message3);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE3, message);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
